<?php
	date_default_timezone_set("Europe/Stockholm");
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
	set_time_limit(0);
	define('DEBUG', true);			# debug print
	define('BIND_IP', '127.0.0.1');		# what ip server will listen to
	define('BIND_PORT', 92);		# what port server will listen to
	define('U_SLEEP', 1000);		# server loop sleep
	define('STREAM_SELECT_US', 0);		# defines time in micro seconds to wait on select_stream before moving on
	define('STREAM_SELECT_S',0);		# defines time in seconds to wait on select_stream before moving on
	define('STATS_FILE','/opt/www/wrimg.com/stats/stats2.html');			# path to the stats file
	define('MAX_RECV', 4096);		# maximum number of bytes to receive on incoming data with fread()
?>
