rsock
=====

rsock - a crazy fast socket server


Performance, stream_* version:
------------------------------------------
On a VPS with spec
Intel(R) Xeon(R) CPU X5650  @ 2.67GHz (single core)

2 GB RAM

It manages to handle appx 500-3300 requests/sec depending on your system configuration.
The range is crazy, as it's very dependent on your system configuration. The most important speed-optimization setting 
in conf/rsock.conf.php is U_SLEEP. Keep in mind though, that lowering this sleep time makes your server load increase as 
it iterates like crazy and does only increase performance to a certain point, the rest is up to your system conf.


To start it:
------------------------------------------
```
/path/to/php/bin/php -f daemons/rsock.php
```

How i performed the tests
------------------------------------------
With 
```
...
define('BIND_IP', '127.0.0.1');
define('BIND_PORT', 92);
define('U_SLEEP', 200);
...
```
in rsock.conf.php - i ran this from a totaly different VPS server:

```
# ab -n 10000 -c 700 http://<my-host>:<my-port>/get_rsock
```

...having my rsock-server proxying through nginx with a simple 
```
worker_processes 1;
events {
   worker_connections 2048;
   multi_accept on;
   use epoll;   
}
http {
   ...
   upstream rsock {
      server 127.0.0.1:92;
   }
   ...
}
```
in nginx-conf and
```
server {
   ...
   location ~ get_rsock {
      proxy_pass http://rsock;
   }
   ...
}
```
to use the upstream.

Apache bench statistics
------------------------------------------
Apache bench seems to have some issues with the finalization of the benchmark (it takes forever to end the benchmark) which gives some invalid req/sec number:
```
This is ApacheBench, Version 2.3 <$Revision: 655654 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking wrimg.com (be patient)
Completed 1000 requests
Completed 2000 requests
Completed 3000 requests
Completed 4000 requests
Completed 5000 requests
Completed 6000 requests
Completed 7000 requests
Completed 8000 requests
Completed 9000 requests
Completed 10000 requests
Finished 10000 requests


Server Software:        nginx/1.1.0
Server Hostname:        <my-host>
Server Port:            80

Document Path:          /get_rsock
Document Length:        182 bytes

Concurrency Level:      700
Time taken for tests:   36.622 seconds
Complete requests:      10000
Failed requests:        12
   (Connect: 0, Receive: 0, Length: 12, Exceptions: 0)
Write errors:           0
Total transferred:      3799328 bytes
HTML transferred:       1817816 bytes
Requests per second:    273.06 [#/sec] (mean)
Time per request:       2563.558 [ms] (mean)
Time per request:       3.662 [ms] (mean, across all concurrent requests)
Transfer rate:          101.31 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:       25   88 430.9     25    3025
Processing:    26 1283 4845.7     59   36587
Waiting:       26 1283 4845.7     59   36587
Total:         51 1372 4861.1     85   36613

Percentage of the requests served within a certain time (ms)
  50%     85
  66%     94
  75%     99
  80%    104
  90%   1529
  95%   7097
  98%  23833
  99%  26886
 100%  36613 (longest request)
```


Internal rsock statistics
------------------------------------------
My internal counter in the server tells a different story (conf:STATS_FILE->.html file):

```
Avarage rcvd/s (last 10 secs):   837 rcvd/s (max: 1384)
Avarage sent/s (last 10 secs):   838 sent/s (max: 1384)
```


Some stuff to look at in your OS:
------------------------------------------

SOMAXCONN
```
sudo sysctl -a | grep somaxconn
(i have been using the default of 128 in ubuntu when performing my tests).
```

Google this subject to find out more ...
