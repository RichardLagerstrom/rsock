<?php
/** 
 * Keeps all client connections (aka, connection pool) and some other data
 * NOTE: the [0] position is always taken by a reference to the listener socket (this must be here or else we will never find out when 
 * someone tries to connect because c_pool is copied into stream_select)
*/
class rsock_clients_pool {
	/**
	 * PROPERTIES
	*/
	public $resources = array();			# <resource> the resource for this connection
	public $connection_time = array();		# <int> unix time() when connection was accepted with $this->accept_one();
	public $ip = array();				# <string> ip address of the connected client
	public $last_sent_health_check = array();	# <int> keeps last time() health check was sent to this user (0 = not initiated yet, -1 = ok reply from server)
}

/** 
 * Main rsock init class
*/
class rsock {
	/**
	 * PROPERTIES
	*/
	private $server_started = 0;		# <unix timestamp> unix time when server was started
	private $stats_reset_time = 0;		# <int> interval when to purge avg/s statistics to make them always be current, instead of "since server started" (resets every 10 secs)
	private $s_pool = null;			# <resource/stream socket on specified local_socket> stores server connection (the listening pool, stream_socket_server())
	private $c_pool = null;			# <object::rsock_clients_pool> one instance of this object which keeps all client connections
	private $shutdown = false;		# <bool> whether to shutdown server
	private $live_stats = 0;		# <int> number of regenerations of the live stats
	private $num_iterations = 0;		# number of iterations that has been made by the socket server listening pool
	private $rcvd_times = 0;		# received times (how many times the server has received data)
	private $rcvd_times_runtime = 0;	# value will reset every 10 secs to keep current number in avarage stats
	private $rcvd_times_record = 0;		# received times record since server start
	private $rcvd_bytes = 0;		# received bytes since server start
	private $rcvd_bytes_runtime = 0;	# value will reset every 10 secs to keep current number in avarage stats
	private $rcvd_bytes_record = 0;		# received bytes record since server start
	private $snt_times = 0;			# sent times (how many times the server has received data)
	private $snt_times_runtime = 0;		# value will reset every 10 secs to keep current number in avarage stats
	private $snt_times_record = 0;		# sent times record since server start
	private $snt_bytes = 0;			# sent bytes since server start
	private $snt_bytes_runtime = 0;		# value will reset every 10 secs to keep current number in avarage stats
	private $snt_bytes_record = 0;		# sent bytes record since server start
	private $connections = 0;		# number of active connections
	private $connections_record = 0;	# the number of active connections record (could be either a logged on user, a notification, or something else connected)
	private $connections_record_date = '0000-00-00 00:00:00';	 # datetime when "connection_record" was taken
	private $health_checks = 0;		# number of health checks made since server start
	private $drop_connections_counter = 0;	#  number of "drop_connections()" made since server start
	private $connection_list = null;	# connection list 
						# keeps a list of all accepted connections, for each connection it shows:
						# 		- Resource ID#
						# 		- Connection time (well, it actually calculates how old the connection 
						# 		  is base on the connection_time... who can interpret 
						# 		  a unix timestamp and get any smarter from reading it?)
						# @var integer
						# @access private
		
	/**
	 * METHODS
	*/
		/**
		 * starts the stream socket server
		 * @access public
		 * @return void
		 */
		public final function __construct() {
			// set start date/time
			$this->server_started = time();
			$this->stats_reset_time = time()-1;

			// start server
			$this->init();
		}

		/**
		 * starts the stream socket server
		 * @access private
		 * @return void
		 */
		private final function init() {
			// create the master listening socket
			$this->s_pool = stream_socket_server('tcp://'.BIND_IP.':'.BIND_PORT, $errno, $errstr);

			// create the client connection keeper
			$this->c_pool = new rsock_clients_pool();
			
			// did we fail on creation?
			if ($this->s_pool === false OR is_null($this->s_pool) OR !is_resource($this->s_pool)) {
				$this->log_error("$errstr ($errno)");
				$this->shutdown = true;
			} else {
				stream_set_blocking($this->s_pool, 0);

				# put reference to server resource into master array's first position [0]
				$this->c_pool->resources[0] = &$this->s_pool;
				$this->c_pool->connect_time[0] = time();	
				$this->c_pool->ip[0] = stream_socket_get_name($this->s_pool, false);
			}

			while (!$this->shutdown) {
				$this->num_iterations++;					# increase number of loops made by the server
				$arr_read = $this->c_pool->resources;				# copy list of connections into read-check (always get new list of resources)
				$arr_exception = $this->c_pool->resources;			# copy list of connections into exception-check (always get new list of resources)
				$arr_write = null;
				$num_changed_streams = stream_select(	$arr_read, 
									$arr_write, 
									$arr_exception, 
									STREAM_SELECT_S, 
									STREAM_SELECT_US);	# waits for streams to change status
				if($num_changed_streams === FALSE)
				{
					//$this->log_error('A warning has been raised in stream_select(). socket_last_error: ' . trim(socket_strerror(socket_last_error())));
					continue;
				}

				$this->handle_exception($arr_exception);			# handles all exceptions found in stream_select()
				$this->handle_read($arr_read);					# handles all reads found in stream_select()

				$this->fix_aborted_clients();			# fixes aborted clients
				//$this->drop_connections();				# drop connections with no valid handshake after TIMEOUT_SECS
				//$this->drop_health_check_noreplies();	# drop health check noreply (within TIMEOUT_HEALTH_CHECK_NOREPLIES_SECS) connections (only handshake OK)
				$this->generate_connection_list();		# generate a list over accepted connections
				$this->generate_live_stats();

				unset($arr_read);
				unset($arr_write);
				unset($arr_exception);
				unset($num_changed_streams);
				usleep(U_SLEEP);
			}
		}

		/**
		 * handle "exceptions" returned from stream_select()
		 * @access private
		 * @param resource-array $arr_exception the array containing all except resources from stream_select()
		 * @return void
		 */
		private final function handle_exception($arr_exception) {
			if(sizeof($arr_exception) > 0) echo date("Y-m-d H:i:s") . ": Exception(s) has occured, specification follows:\n";
			foreach($arr_exception as $sock) {
				echo date("Y-m-d H:i:s") . 'Exception occured on: ' . stream_socket_get_name($sock, true) . "\n";
				//$this->log_error('Exception occured on: ' . stream_socket_get_name($sock, true));	# log exception
			}

			if(isset($sock)) unset($sock);
			unset($arr_exception);
		}

		/**
		 * handle "reads" returned from stream_select()
		 * @access private
		 * @param resource-array $arr_read the array containing all read resources from stream_select()
		 * @return void
		 */
		private final function handle_read($arr_read) {
			foreach($arr_read as $sock) {
				if($sock === $this->s_pool) {						# incoming connection, deal with it
					$this->accept_one($this->s_pool);				# accept incoming connection request

				} else {
					$sock_data = trim(stream_socket_recvfrom($sock, MAX_RECV));	# receive data from client

					if(strlen($sock_data) === 0) {					# connection was closed
						$this->disconnect_one($sock);				# disconnect this client

					} else if($sock_data === false) {				# an error occured on fread()
						echo date("Y-m-d H:i:s") . ": An error occured in fread() while reading socket: $sock \n";
						//$this->log_error('An error occured in' . 'fread() while reading socket: ' . $sock);
						$this->disconnect_one($sock);				# disconnect this client

					} else {
						$this->rcvd_bytes += strlen($sock_data);		# increase received bytes
						$this->rcvd_bytes_runtime += strlen($sock_data);	# increase received bytes
						$this->rcvd_times++;					# increase received times
						$this->rcvd_times_runtime++;				# increase received times
						if (DEBUG) echo "RECEIVED DATA\n";

						// check for rcvd record
						$time_gone = (time()-$this->stats_reset_time);
						$time_gone = $time_gone > 0 ? $time_gone : 1;
						$tmp = $this->rcvd_times_runtime/$time_gone;
						if($this->rcvd_times_record < $tmp) $this->rcvd_times_record = $tmp;
						$tmp = $this->rcvd_bytes_runtime/$time_gone;
						if($this->rcvd_bytes_record < $tmp) $this->rcvd_bytes_record = $tmp;
						$this->send_data($sock, $sock_data);
						$this->disconnect_one($sock);
						unset($tmp);															
					}
					
					unset($sock_data);
				}
			}
			
			unset($sock);
			unset($arr_read);
		}

		/**
		 * accept given socket (client)
		 * @access private
		 * @param resource $in_socket socket to accept
		 * @return void
		 */
		private final function accept_one($in_socket) {
			// accept new connection
			if($new_sock = @stream_socket_accept($in_socket))
			{
				// grab clients ip address
				$ip = stream_socket_get_name($new_sock, true);
				
				// always make sure to keep keys in numerical order. Right: 0,1,2,3 .. Wrong: 0,4,125,475
				// 	this is done to prevent key integer to grow bigger for each new connection.
				// 	eg, 1 000 000 connections (with a current number of active clients of 83) since server
				// 	started would return in an index of 1 000 000, instead of 83. We want 83, not 1 000 000.
				// 	so many array indicies, would start chewing to much RAM (or reach 32bit int limit) on the server unless server is getting rebooted 
				// 	every now and then, we all know we don't ever want to reboot the server.		
				if(isset($this->c_pool->resources)) $this->c_pool->resources = array_values($this->c_pool->resources);	
				if(isset($this->c_pool->connect_time)) $this->c_pool->connect_time = array_values($this->c_pool->connect_time);			
				if(isset($this->c_pool->ip)) $this->c_pool->ip = array_values($this->c_pool->ip);
				if(isset($this->c_pool->last_sent_health_check)) $this->c_pool->last_sent_health_check = array_values($this->c_pool->last_sent_health_check);

				$this->c_pool->resources[] = $new_sock;		# put client resource into connection pool
				$this->c_pool->usernames[] = '';		# put client username into connection pool (none available yet, client will send 
										# his/hers username as soon as connection has been established, hopefully)
				$this->c_pool->mids[] = '';			# put client mid into connection pool (none available yet, client will send 
										# his/hers mid as soon as connection has been established, hopefully)
				$this->c_pool->connect_time[] = time();		# keep "connection accepted time" for later usage (to drop connections 
										# which has no "ok handshake" within TIMEOUT_SECS).
				$this->c_pool->ip[] = $ip;			# keep clients ip address
				$this->c_pool->last_sent_health_check[]	= 0;
				
				$this->connections++;				# increase number of accepted connection (logged on user, or just a notification)
				if($this->connections > $this->connections_record)
				{
					$this->connections_record = $this->connections;			# set new record of active connections
					$this->connections_record_date = date("Y-m-d H:i:s");		# set new connection record date
				}
				unset($new_sock);
				unset($in_socket);
			}
			else 
			{
				// feedback to log file
				echo 'Could not accept connection (too many open files?).';
				
				// shut down server
				$this->shut_serv();
			}
		}

		/**
		 * disconnects given socket (probably a client, but it can and will shutdown the server if given)
		 * @access private
		 * @param resource $in_socket the socket to disconnect
		 * @return void
		 */
		private final function disconnect_one($in_socket) {
			fclose($in_socket);
			
			$key = array_search($in_socket, $this->c_pool->resources, TRUE);	# get clients key in connection pool array
			
			// remove client from connection pool and resort the key index
			if($key !== false AND isset($this->c_pool->resources[$key])) 
			{
				// unset connection pool for this connection
				if(isset($this->c_pool->resources[$key])) unset($this->c_pool->resources[$key]);		# unset resource keeper for this connection
				if(isset($this->c_pool->connect_time[$key])) unset($this->c_pool->connect_time[$key]);		# unset connect_time keeper for this connection
				if(isset($this->c_pool->last_sent_health_check[$key])) {
					unset($this->c_pool->last_sent_health_check[$key]);					# unset last_sent_health_check keeper for this connection
				}
				
				// resort index
				if(isset($this->c_pool->resources) AND is_array($this->c_pool->resources)) {
					// resort the key index (see accept_one() for a thorough explanation on this action, why we use it)
					$this->c_pool->resources = array_values($this->c_pool->resources);
				}
				if(isset($this->c_pool->connect_time) AND is_array($this->c_pool->connect_time)) {
					// resort the key index (see accept_one() for a thorough explanation on this action, why we use it)
					$this->c_pool->connect_time = array_values($this->c_pool->connect_time);
				}
				if(isset($this->c_pool->last_sent_health_check) AND is_array($this->c_pool->last_sent_health_check)) {
					// resort the key index (see accept_one() for a thorough explanation on this action, why we use it)
					$this->c_pool->last_sent_health_check = array_values($this->c_pool->last_sent_health_check);
				}
			}
			$this->connections--;
			unset($in_socket);
		}

		/**
		 * sends given message to given socket resource in JSON
		 * @access private
		 * @param resource $in_socket the socket to send data to
		 * @param string $in_data the data to send to given socket
		 * @return void
		 */
		private final function send_data($in_socket, $in_data)
		{
			if (DEBUG) echo "SEND DATA ($in_data)\n";

			$this->snt_bytes += strlen($in_data);
			$this->snt_bytes_runtime += strlen($in_data);
			$this->snt_times++;
			$this->snt_times_runtime++;

			// check for snt record
			$time_gone = (time()-$this->stats_reset_time);
			$time_gone = $time_gone > 0 ? $time_gone : 1;
			$tmp = $this->snt_times_runtime/$time_gone;
			if($this->snt_times_record < $tmp) $this->snt_times_record = $tmp;

			$tmp = $this->snt_bytes_runtime/$time_gone;
			if($this->snt_bytes_record < $tmp) $this->snt_bytes_record = $tmp;

			// ------------------------------------------
			// Headers
			// ------------------------------------------
				// JSON
				$headers = "HTTP/1.1 200\r\n";
				$headers .= "Content-Type: application/json;charset=utf-8\r\n";
				$headers .= "Content-Length: ".strlen($in_data)."\r\n";
				$headers .= "X-Powered-By: rsock v0.8 (rsock.com)\r\n";
				$headers .= "Connection: close\r\n\r\n";
				if (fwrite($in_socket, $headers.$in_data) === false) {
					echo "Could not send to socket.\n";
				}

			unset($in_socket);
			unset($in_data);
			unset($time_gone);
			unset($tmp);
			unset($headers);
		}

		/**
		 * sends given message to given socket resource in XML format, encrypts USER and TEXT attributes
		 * @access private
		 * @param resource $in_socket the socket to send data to
		 * @param string $in_usr the username who sent this data
		 * @param string $in_data the data to send to given socket
		 * @param string $in_type what type of data is this (0 = health check, 1 = server message, 2 = blink relation etc)
		 * @return void
		 */
		private final function send_one($in_socket, $data)
		{
			$this->snt_bytes += strlen($data);						# increase sent bytes
			$this->snt_bytes_runtime += strlen($data);					# increase sent bytes
			$this->snt_times++;								# increase sent times
			$this->snt_times_runtime++;							# increase sent times

			// check for snt record
			$time_gone = (time()-$this->stats_reset_time);
			$time_gone = $time_gone > 0 ? $time_gone : 1;
			$tmp = $this->snt_times_runtime/$time_gone;
			if($this->snt_times_record < $tmp) $this->snt_times_record = $tmp;

			$tmp = $this->snt_bytes_runtime/$time_gone;
			if($this->snt_bytes_record < $tmp) $this->snt_bytes_record = $tmp;

			@stream_socket_sendto($in_socket,$data.chr(0));					# send data to given socket
			unset($in_socket);
			unset($data);
			unset($time_gone);
			unset($tmp);															
		}

		/**
		 * sends a test byte to all clients just to find those who has aborted without notice (e.g. network failure)
		 * @access private
		 * @return void
		 */
		private final function fix_aborted_clients()
		{
			$rand = rand(1,10000);								# set possibility for this to run
			if($rand == 50) {									# if 50, then send test bit to all clients just to see if they are there
				$this->health_checks++;							# increase number of health checks made
						
				$send_count = 0;
				foreach($this->c_pool->resources as $key => $sock) {			# loop all clients
					if($key == 0) continue;						# excludes the server socket
					$this->send_one($sock, 't');					# send test bit to client (if client is 
													# down, a "(strlen($sock_data) == 0)" will be raised)
					$this->c_pool->last_sent_health_check[$key] 	= time();	# save time() of when health check was last sent to this member 
													# (to be able to disconnect him/her if no reply arrives within time_out secs)
					$send_count++;
				}
				
				unset($key);													# unset used variables
				unset($sock);
				
				echo date("Y-m-d H:i:s") . ": [".$this->convert_bytes(memory_get_usage())."] Health check, did send $send_count checks (removed clients whoms connection died or who just aborted for some reason).\n";
			}
			
			unset($rand);														# unset used variables
		}

		/**
		 * shuts down the stream server by first disconnecting all clients, then close it self
		 * @access private
		 * @return void
		 */
		private final function shut_serv() {
			foreach($this->c_pool->resources as $key => $sock)		# loop all clients
			{
				if($key == 0) continue;					# exclude the listener socket
				$this->disconnect_one($sock);				# disconnect this client
			}
			
			// shutdown server
			if(isset($this->s_pool)) {
				$this->disconnect_one($this->s_pool); $this->shutdown = true;
			}
			
			unset($key);
			unset($sock);
		}

		/**
		 * takes bytes as parameter and converts it to appropriate B, Kb, Mb, Gb, Tb
		 * @access private
		 * @return void
		 */
		private final function convert_bytes($bytes) {
			switch($bytes) {
				// less than 1 Kb (bytes)
				case $bytes < 1024:
					$bytes = number_format($bytes, 2, ',', ' ') . ' Bytes';								# BYTES
				break;
				
				// more than 1 Kb, but less than 1000 Kb (kilo bytes)
				case ($bytes >= 1024 AND $bytes < 1024000):
					$bytes = number_format(($bytes/1024), 2, ',', ' ') . ' Kb';							# KILO BYTE
				break;
				
				// more than 1000 Kb, but less than 1000 Mb (mega bytes)
				case ($bytes >= 1024000 AND $bytes < 1024000000):
					$bytes = number_format(($bytes/1024/1024), 2, ',', ' ') . ' Mb';						# MEGA BYTE
				break;
				
				// more than 1000Mb, but less than 1000Gb (giga bytes)
				case ($bytes >= 1024000000 AND $bytes < 1024000000000):
					$bytes = number_format(($bytes/1024/1024/1024), 2, ',', ' ') . ' Gb';						# GIGA BYTE
				break;
				
				// more than 1000Gb, but less than 1000Tb (tera bytes)
				case ($bytes >= 1024000000000 AND $bytes < 1024000000000000):
					$bytes = number_format(($bytes/1024/1024/1024/1024), 2, ',', ' ') . ' Tb';					# TERA BYTE
				break;
				
				// more than 1000Tb, but less than 1000Pb (peta bytes)
				case ($bytes >= 1024000000000000 AND $bytes < 1024000000000000000):
					$bytes = number_format(($bytes/1024/1024/1024/1024/1024), 2, ',', ' ') . ' Pb (peta byte)';			# PETA BYTE
				break;
				
				// more than 1000Pb, but less than 1000Eb (exa bytes)
				case ($bytes >= 1024000000000000000 AND $bytes < 1024000000000000000000):
					$bytes = number_format(($bytes/1024/1024/1024/1024/1024/1024), 2, ',', ' ') . ' Eb (exa byte)';			# EXA BYTE
				break;
				
				// more than 1000Eb, but less than 1000Zb (zetta bytes)
				case ($bytes >= 1024000000000000000000 AND $bytes < 1024000000000000000000000):
					$bytes = number_format(($bytes/1024/1024/1024/1024/1024/1024/1024), 2, ',', ' ') . ' Zb (zetta byte)';		# ZETTA BYTE
				break;
				
				// more than 1000Zb (yotta bytes)
				case ($bytes >= 1024000000000000000000000):
					$bytes = number_format(($bytes/1024/1024/1024/1024/1024/1024/1024/1024), 2, ',', ' ') . ' Yb (yotta byte)';	# YOTTA BYTE
				break;
			}
			
			return $bytes;
		}

		/**
		 * sends a test byte to all clients just to find those who has aborted without notice (e.g. network failiure)
		 * @access private
		 * @return void
		 */
		private final function generate_connection_list()
		{
			$rand = rand(1,200);		# set possibility for this to run
			if($rand == 50) {		# if 50, then send test bit to all clients just to see if they are there
				$this->connection_list = '<table cellspacing="0" cellpadding="0" border="0">';
				$this->connection_list .= "<tr>
									<td style='width: 200px; font-size: 10px;'><b>Resource ID</b></td>
									<td style='width: 150px; font-size: 10px;'><b>Username (mid)</b></td>
									<td style='width: 100px; font-size: 10px;'><b>Connection age</b></td>
									<td style='width: 120px; font-size: 10px;'><b>Last health check</b></td>
									<td style='width: 100px; font-size: 10px;'><b>IP Address</b></td>
							   </tr>";
				
				// iterate all clients
				foreach($this->c_pool->resources as $key => $sock) {
					// grab values
					$resource 	= isset($this->c_pool->resources[$key]) 		? $this->c_pool->resources[$key] : 'no resource id';
					$conn_age 	= isset($this->c_pool->connect_time[$key]) 		? (time()-$this->c_pool->connect_time[$key]) : '';
					$ip_address	= isset($this->c_pool->ip_address[$key]) 		? $this->c_pool->ip_address[$key] : '';
					$last_hc 	= isset($this->c_pool->last_sent_health_check[$key]) 	? ($this->c_pool->last_sent_health_check[$key] > 0 ? 
															(time()-$this->c_pool->last_sent_health_check[$key])
														: 
															$this->c_pool->last_sent_health_check[$key]) : '';
					
					$this->connection_list .= "<tr>
													<td style='font-size: 10px;'>$resource (key: $key)</td>
													<td style='font-size: 10px;'>".($key == 0 ? "Server, listener socket" : 'Client connected')."</td>
													<td style='font-size: 10px;'>$conn_age secs</td>
													<td style='font-size: 10px;'>$last_hc</td>
													<td style='font-size: 10px;'>$ip_address</td>
											   </tr>";
				}
					
				$this->connection_list .= "</table>";
				
				
				unset($key);												# unset used variables
				unset($sock);
			}
			
			unset($rand);													# unset used variables
		}

		/**
		 * generates statistics about this server
		 * @access private
		 * @return void
		 */
		private final function generate_live_stats() {
			$rand = rand(1,250);											# set possibility for this to run
			if($rand == 50)													# if 50, then send test bit to all clients just to see if they are there
			{	
				if(time() > $this->server_started)
				{
					$this->live_stats++;									# increase number of times live stats has been regenerated
					
					$time_gone = (time()-$this->stats_reset_time);
					$time_gone = $time_gone > 0 ? $time_gone : 1;

					$html = '<html>';
					$html .= '<head>';
					$html .= '	<title>';
					$html .= '		rsearch live stats';
					$html .= '	</title>';
					$html .= '</head>';
					$html .= '<body>';
					$html .= '	<style> .body {font-family:verdana; font-size:12px;} </style>';
					$html .= '	<style> .td {font-family:verdana; font-size:12px;} </style>';
					$html .= '	<style> .topBorder {font-family:verdana; padding:5px; font-size:12px; border-top:1px solid #bbbbbb;} </style>';
					$html .= '	<style> .header {font-family:verdana; font-size:16px;} </style>';
					$html .= '	<style> .mini {font-family:verdana; font-size:10px;} </style>';
					$html .= '	<table cellspacing="0" cellpadding="0" border="0">';
					$html .= '	<tr>';
					$html .= '		<td colspan="4" class="header"><b>rsearch live stats</b><br/><br/></td>';
					$html .= '	</tr>';
					$html .= '	<tr>';
					$html .= '		<td class="topBorder"><b>Server started:</b></td>';
					$html .= '		<td colspan="3" class="topBorder">'.date("Y-m-d \a\\t H:i:s", $this->server_started).'</td>';
					$html .= '	</tr>';
					$html .= '	<tr>';
					$html .= '		<td style="width:130px;" class="topBorder"><b>Server iterations:</b></td>';
					$html .= '		<td style="width:150px;" class="topBorder">'.number_format($this->num_iterations, 0, ',', ' ').' loops</td>';
					$html .= '		<td style="width:130px;" class="topBorder"><b>Iterations/sec:</b></td>';
					$html .= '		<td class="topBorder">'.number_format($this->num_iterations/(time() - $this->server_started), 0, ',', ' ').' loops/s</td>';
					$html .= '	</tr>';
					$html .= '	<tr>';
					$html .= '		<td style="width:130px;" class="topBorder"><b>Regenerated stats:</b></td>';
					$html .= '		<td style="width:150px;" class="topBorder">'.number_format($this->live_stats, 0, ',', ' ').' times</td>';
					$html .= '		<td style="width:130px;" class="topBorder"><b>Health checks:</b></td>';
					$html .= '		<td class="topBorder">'.number_format($this->health_checks, 0, ',', ' ').' pcs</td>';
					$html .= '	</tr>';
					$html .= '	<tr>';
					$html .= '		<td style="width:130px;" class="topBorder"><b>Memory usage:</b></td>';
					$html .= '		<td style="width:150px;" class="topBorder">'.round((memory_get_usage()/1024/1024),1).' Mb</td>';
					$html .= '		<td style="width:130px;" class="topBorder"><b>Memory peak:</b></td>';
					$html .= '		<td class="topBorder">'.round((memory_get_peak_usage()/1024/1024),1).' Mb</td>';
					$html .= '	</tr>';
					$html .= '	<tr>';
					$html .= '		<td class="topBorder"><b>Active connections:</b></td>';
					$html .= '		<td class="topBorder">'.number_format($this->connections, 0, ',', ' ').'</td>';
					$html .= '		<td class="topBorder"><b>Active connections record:</b></td>';
					$html .= '		<td class="topBorder">'.number_format($this->connections_record, 0, ',', ' ').', date: '.$this->connections_record_date.'</td>';
					$html .= '	</tr>';
					$html .= '	<tr>';
					$html .= '		<td class="topBorder"><b>Has received data:</b></td>';
					$html .= '		<td class="topBorder">'.number_format($this->rcvd_times, 0, ',', ' ').' times</td>';
					$html .= '		<td class="topBorder"><b>Avarage rcvd/s (last 10 secs):</b></td>';
					$html .= '		<td class="topBorder">'.number_format($this->rcvd_times_runtime/$time_gone, 0, ',', ' ').' rcvd/s (max: '.round($this->rcvd_times_record, 0).')</td>';
					$html .= '	</tr>';
					$html .= '	<tr>';
					$html .= '		<td class="topBorder"><b>Has sent data:</b></td>';
					$html .= '		<td class="topBorder">'.number_format($this->snt_times, 0, ',', ' ').' times</td>';
					$html .= '		<td class="topBorder"><b>Avarage sent/s (last 10 secs):</b></td>';
					$html .= '		<td class="topBorder">'.number_format($this->snt_times_runtime/$time_gone, 0, ',', ' ').' ('.$this->snt_times_runtime.'/'.$time_gone.') sent/s (max: '.round($this->snt_times_record, 0).')</td>';
					$html .= '	</tr>';
					$html .= '	<tr>';
					$html .= '		<td class="topBorder"><b>Bytes received:</b></td>';
					$html .= '		<td class="topBorder">'.$this->convert_bytes($this->rcvd_bytes).'</td>';
					$html .= '		<td class="topBorder"><b>Bytes sent:</b></td>';
					$html .= '		<td class="topBorder">'.$this->convert_bytes($this->snt_bytes).'</td>';
					$html .= '	</tr>';
					$html .= '	<tr>';
					$html .= '		<td class="topBorder"><b>Download avarage:</b></td>';
					$html .= '		<td class="topBorder">'.$this->convert_bytes(($this->rcvd_bytes_runtime/$time_gone)).'/s (max: '.$this->convert_bytes($this->rcvd_bytes_record).')</td>';
					$html .= '		<td class="topBorder"><b>Upload avarage:</b></td>';
					$html .= '		<td class="topBorder">'.$this->convert_bytes(($this->snt_bytes_runtime/$time_gone)).'/s (max: '.$this->convert_bytes($this->snt_bytes_record).')</td>';
					$html .= '	</tr>';
					$html .= '	<tr>';
					$html .= '		<td class="topBorder" title="Number of times drop_connections() has been run. This disconnects connections with no valid handshake after TIMEOUT_SECS seconds."><b>Drop connection runs:</b></td>';
					$html .= '		<td class="topBorder" colspan="3">'.$this->drop_connections_counter.' times</td>';
					$html .= '	</tr>';
					$html .= '	<tr>';
					$html .= '		<td class="topBorder" colspan="4"><b>Accepted connections:</b></td>';
					$html .= '	</tr>';
					$html .= '	<tr>';
					$html .= '		<td class="topBorder" colspan="4">'.$this->connection_list.'</td>';
					$html .= '	</tr>';
					//$html .= '	<tr>';
					//$html .= '		<td valign="top" class="topBorder"><b>Users online:</b></td>';
					//$html .= /*'		<td colspan="3" valign="top" class="topBorder">Disabled due to shitloads of users</td>';*/	'<td colspan="4" valign="top" class="topBorder">'.str_replace(',', '<br/>- ', $this->get_all_online()).'</td>';
					//$html .= '	</tr>';
					$html .= '	<tr>';
					$html .= '		<td colspan="4">&nbsp;</td>';
					$html .= '	</tr>';
					$html .= '	<tr>';
					$html .= '		<td colspan="4" class="mini"><br/><br/><u>Last updated</u>: '.date("Y-m-d H:i:s", time()).'<br/><br/></td>';
					$html .= '	</tr>';
					$html .= '	</table>';
					$html .= '</body>';
					$html .= '</html>';
				
					// reset stats data
					if((time()-$this->stats_reset_time) > 10)
					{
						$this->rcvd_times_runtime = 0;
						$this->snt_times_runtime = 0;
						$this->rcvd_bytes_runtime = 0;
						$this->snt_bytes_runtime = 0;
						$this->stats_reset_time = time();
					}  

					// W: writing, truncate file to zero length, create if not already created.
					// B: "in contrast, you can also use 'b' to force binary mode, which will not translate your data."
					$fp = fopen(STATS_FILE, "wb");
					
					// make sure we got a handle
					if($fp)
					{
						fwrite($fp, stripslashes($html), strlen($html));
						fclose($fp);
					}
				}
			}
		}
}
?>
